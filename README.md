# formative15

### Nexsoft Fun Coding Bootcamp Batch 7 Day 15

## Author
### Bayu Seno Ariefyanto

## 1. Merging Task
#### A. Merging Branch Log
![Merging Branch Log](merging-branch-log.png)

#### B. Merging Branch Graph
![Merging Branch Graph](merging-branch-graph.png)


## 2. Creating Conflict Task
#### A. Conflict On Push, Rejected
![Conflict Pull Rejected](conflict-push-rejected.png)
#### B. Conflict On Pull, Conflict
![Conflict Pull Rejected](conflict-pull-conflict.png)
#### C. Conflict Merged
![Win Condition](conflict-merged.png)
